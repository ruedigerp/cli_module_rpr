packagename: rpr 
name: rpr test package
version: 0.5.3
description: test Package Manager 

Paketliste aktualisieren:
#> hmg opm update

Verfügbare Pakete anzeigen
#> hmg opm list 
1: dns - DNS update BIND9 and Cloudflare DNS API (rkuepper)
2: config_example - Konfiguration für das Module anlegen, anzeigen und im Modul benutzen (rkuepper)
3: example - Beispiel Modul (totlik)

Paket installieren
#> hmg opm install dns


