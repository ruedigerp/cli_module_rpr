
import os, sys, errno
import zipfile
import shutil
from pathlib import Path
# import urllib
from urllib.request import urlretrieve

module_name="rpr"
module_version = "0.5.8"
commands = ["help", "create", "upload", "version" ]
configfile = '~/.hmgcli.d/'+ module_name+ '.conf'
configkeys = ["url", "user", "password"]
cfg = {}

def execute(command,params):
    if command == "create":
        pack_create()
    elif command == "upload":
        pack_upload(params)
    elif command == "help":
        opm_help()
    elif command == "version":
        version()
    elif command == "info":
        opm_info(params)

def version():
    print (module_name, " Version: ", module_version)



